<div class="wrap">
    <h2><?php _e('Yelp Term Finder', 'wp-ytf') ?></h2>

    <form method="post">
        <div>
            <label><?php _e('Location', 'wp-ytf') ?></label>
            <input type="text" name="location" value="<?php echo (!empty($editArr)) ? $editArr->location : "" ?>" style="width: 25%;"/>
            <label><?php _e('Radious', 'wp-ytf') ?></label>
            <input type="text" name="radious" value="<?php echo (!empty($editArr)) ? $editArr->radious : "" ?>" style="width: 25%;"/>
            <input name="submit" id="submit" class="button button-primary" value="Save Changes" type="submit">
        </div>
    </form>

    <table class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <td><?php _e('Locations', 'my-ytf') ?></td>
                <td><?php _e('Radious', 'my-ytf') ?></td>
            </tr>
        </thead>
        <tbody id="the-list">
            <?php foreach ($locationsArr as $key => $value) { ?>
                <tr>
                    <td>
                        <strong><a class="row-title" href="<?php echo admin_url('admin.php?page=ytf-dashboard&edit=').$value->id ; ?>"><?php echo $value->location; ?></a></strong>
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <div class="row-actions">
                            <span class="edit"><a href="<?php echo admin_url('admin.php?page=ytf-dashboard&edit=').$value->id ; ?>">Edit</a> | </span>
                            <span class="inline hide-if-no-js"><a href="<?php echo admin_url('admin.php?page=ytf-dashboard&del=').$value->id ; ?>" class="editinline">Delete</a> </span>
                        </div>

                    </td>
                    <td><?php echo $value->radious; ?></td>

                </tr>

            <?php } ?>
        </tbody>

    </table>
</div>