<?php

/*
  Plugin Name: Yelp Term Finder
  Plugin URI: http://rgknowledgebase.wordpress.com
  Description: Find the terms based on locations
  Version: 1.0
  Author: Ravi Gupta
  Author URI: https://www.linkedin.com/in/ravigupta112
  Text Domain: wp-ytf
 */

class WP_YTF {

    /**
     * Constructor
     */
    public function __construct() {

        add_action('admin_menu', array($this, 'wpytf_add_menu'));
        register_activation_hook(__FILE__, array($this, 'wpytf_install'));
        register_deactivation_hook(__FILE__, array($this, 'wpytf_uninstall'));
        add_action('admin_init', array(&$this, 'ytf_plugin_settings'));
        add_shortcode('wp_ytf', array(&$this, 'ytf_short_code'));
    }

    /*
     * Actions perform at loading of admin menu
     */

    public function wpytf_add_menu() {
        add_menu_page('YTF', 'YTF', 'manage_options', 'ytf-dashboard', array(&$this, 'wpytf_page_file_path'), plugins_url('images/wp-analytics-logo.png', __FILE__), '2.2.9');
        add_submenu_page('ytf-dashboard', 'YTF' . ' Dashboard', ' Dashboard', 'manage_options', 'ytf-dashboard', array(&$this, 'wpytf_page_file_path'));
        add_submenu_page('ytf-dashboard', 'YTF' . ' Settings', '<b style="color:#f9845b">Settings</b>', 'manage_options', 'ytf-settings', array(&$this, 'wpytf_setting'));
    }

    public function ytf_short_code($atts) {
        $args = shortcode_atts(array(
            'class' => ' ',
            'id' => '',
            'map_height' => '400px',
            'map_width' => '500px',
                ), $atts);
        $location = isset($_REQUEST['ytf_location']) ? $_REQUEST['ytf_location'] : "";
        $term = isset($_REQUEST['ytf_term']) ? $_REQUEST['ytf_term'] : "";
        $limit = isset($_REQUEST['ytf_limit']) ? $_REQUEST['ytf_limit'] : 20;
        
        $content = '<div class="ytf_area ' . $class . '">';
        $content .= '<form method="get" id="ytf_frm">';
        $content .= $this->wpytf_location_element($location);
        $content .= $this->wpytf_term_element($term, $limit);
        $content .= '<input type ="submit" value="' . __('Search', 'wp-ytf') . '">';
        $content .= '</form>';
        $content .= '<div/>';
        $content .= '<div class="ytf_location">';
        $content .= $this->wpytf_map($args['map_height'], $args['map_width'], $_REQUEST);
        $content .= '</div>';
        return $content;
    }

    public function wpytf_location_element($location =  "") {

        $locationArr = $this->wpytf_read();

        $select = '<label>Near : </label>';
        $select .= '<select name="ytf_location">';
        $select .= '<option value="">' . __('Select Location', 'wp-ytf') . '</option>';
        foreach ($locationArr as $value) {
            if($location == $value->location){
                $select .= '<option value="' . $value->location . '" selected>' . $value->location . '</option>';
                continue;
            }
            $select .= '<option value="' . $value->location . '">' . $value->location . '</option>';
        }
        $select .= '</select> &nbsp;';
        return $select;
    }

    public function wpytf_term_element($term = "", $limit = 20) {
        $html = '<label> Find : </label>';
        $html .=  '<input type="text" name="ytf_term" id="ytf_term" value="'.$term.'">';
        $html .= '<input type="hidden" name="ytf_limit" id="ytf_limit" value="'.$limit.'">';
        return $html;
    }

    private function wpytf_map($height, $width, $request= array()) {
        $request = array_map('trim', $request);
        $location = (isset($request['ytf_location']) && trim($request['ytf_location'])!= "") ? $request['ytf_location'] : ""; 
        $term = (isset($request['ytf_term']) && trim($request['ytf_term'])!= "") ? $request['ytf_term'] : ""; 
        $limit = (isset($request['ytf_limit']) && trim($request['ytf_limit'])!= "") ? intval($request['ytf_limit']) : 0; 
        if($location == "" || $term == ""){
            return "Please fill search criteria";
        }
        $radious = $this->wpytf_radious($location);

        require_once(__DIR__ . '/yelp-api-master/v2/search.php');
        $yelp = new Yelp_v2();
        $response = json_decode($yelp->search($term, $location, $radious, $limit));
        $info = $locationArr = array();
        foreach ($response->businesses as $key => $value) {
            $info[] = array(
                'address'=> implode(", ", $value->location->display_address),
                'phone' => $value->display_phone,
                'rating' => $value->rating_img_url_small,
                'img' => $value->image_url,
                'url' => $value->url,

            );
            $locationArr[] = array(
                $value->name, 
                $value->location->coordinate->latitude, 
                $value->location->coordinate->longitude, 
                $key+1, 
            );
        }
        
        $currentLatLong[] = $response->region->center->latitude;
        $currentLatLong[] = $response->region->center->longitude;
        
        $html = '<div id="map" style="width: '.$width.'; height: '.$height.';"></div>';
        $html .= '<link rel="stylesheet" href="'.plugins_url('css/style.css', __FILE__).'" type="text/css" media="all">';
        $html .= '<script src="http://maps.google.com/maps/api/js?key='.esc_attr( get_option('google_map_api') ).'" 
          type="text/javascript"></script>';
        $html .= '<script type="text/javascript">
    var locations = '. json_encode($locationArr).';
    var info = '. json_encode($info).';

    var map = new google.maps.Map(document.getElementById("map"), {
      zoom: 14,
      center: new google.maps.LatLng('. implode(",", $currentLatLong).'),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    marker = new google.maps.Marker({
            position: new google.maps.LatLng('. implode(",", $currentLatLong).'),
            map: map,
            animation: google.maps.Animation.DROP,
            title: "'.$location.'",
            icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|1555a8|FFFFFF"
      });

      
    for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            animation: google.maps.Animation.DROP,
            title: locations[i][0],
            icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld="+locations[i][3]+"|FF0000|000000"
      });
      var content = "<div id=\"ytf_info_window\"><div class=\"title\"><a href=\""+info[i].url+"\">"+locations[i][0]+"</a></div>" +
                        "<div class=\"left\">"+
                        "<address>"+info[i].address+"</address>"+
                        "<div class=\"phone\"> Ph. "+info[i].phone+"</div>"+
                        "<div class=\"rate\"><img src=\""+info[i].rating+"\"></div>"+
                        "</div>"+ 
                        "<div class=\"right\"> <img src=\""+info[i].img+"\"> </div>"+
                    "</div>";
      google.maps.event.addListener(marker, "click", (function(marker, i, content) {
        return function() {
          infowindow.setContent(content);
          infowindow.open(map, marker);
        }
      })(marker, i, content));
    }
  </script>';
        return $html;
    }

    /*
     * Actions perform on loading of menu pages
     */

    public function wpytf_page_file_path() {
        $editArr = array();
        if (isset($_GET['del'])) {
            $this->wpytf_delete($_GET['del']);
            echo'<script> window.location="' . admin_url('admin.php?page=ytf-dashboard') . '"; </script> ';
            exit;
        }

        if (isset($_GET['edit']) && isset($_POST['location']) && trim($_POST['location']) != "") {
            $this->wpytf_update($_POST['location'], $_POST['radious'], $_GET['edit']);
            echo'<script> window.location="' . admin_url('admin.php?page=ytf-dashboard') . '"; </script> ';
            exit;
        } else if (isset($_GET['edit'])) {
            $editArr = $this->wpytf_read($_GET['edit']);
        }

        if (isset($_POST['location']) && trim($_POST['location']) != "") {
            $this->wpytf_insert($_POST['location'], $_POST['radious']);
        }

        $locationsArr = $this->wpytf_read();
        require_once(__DIR__ . '/dashboard.php');
    }

    /*
     * Actions perform on loading of menu pages
     */

    public function wpytf_setting() {
        require_once(__DIR__ . '/settings.php');
    }

    public function ytf_plugin_settings() {
        register_setting('ytf-settings-group', 'google_map_api');
        register_setting('ytf-settings-group', 'yelp_consumer_key');
        register_setting('ytf-settings-group', 'yelp_consumer_secret');
        register_setting('ytf-settings-group', 'yelp_token');
        register_setting('ytf-settings-group', 'yelp_token_secret');
    }

    /*
     * Actions perform on activation of plugin
     */

    public function wpytf_install() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "ytf`(
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `location` text NOT NULL,
          `radious` text,
          PRIMARY KEY  (`id`))$charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    /*
     * Actions perform on de-activation of plugin
     */

    public function wpytf_uninstall() {
        
    }

    public function wpytf_insert($string, $radious) {
        global $wpdb;
        if (trim($string) == "") {
            return false;
        }
        $res = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ytf WHERE location = '{$string}'");
        if (!empty($res)) {
            return false;
        }
        $wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}ytf (location, radious) VALUES (%s, %s)", $string, $radious));

        return true;
    }

    public function wpytf_update($string,$radious,  $id) {
        global $wpdb;
        if (trim($string) != "" && intval($id) > 0) {
            $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}ytf 
                                    SET location = %s,
                                        radious = %s
                                    WHERE id = %d", $string, $radious,  $id));
            return true;
        }
        return false;
    }

    public function wpytf_delete($id) {
        global $wpdb;
        if (intval($id) > 0) {
            $wpdb->delete($wpdb->prefix . "ytf", array('id' => $id), array('%d'));
            return true;
        }
    }

    public function wpytf_read($id = 0) {
        global $wpdb;
        $result = array();
        if (intval($id) > 0) {
            $res = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ytf WHERE id = " . $id);
            if (!empty($res)) {
                $result = $res;
            }
            return $result;
        }
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ytf ORDER BY id DESC");
        if (!empty($res)) {
            $result = $res;
        }
        return $result;
    }

    public function wpytf_radious($location = "") {
        global $wpdb;
        $result = "";
        if (trim($location) != "") {
            $res = $wpdb->get_row("SELECT radious FROM {$wpdb->prefix}ytf WHERE location = '{$location}'");
            if (!empty($res)) {
                return $res->radious;
            }
        }
        return $result;
    }

}

new WP_YTF();
