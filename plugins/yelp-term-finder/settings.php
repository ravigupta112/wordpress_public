<div class="wrap">
<h2>YTF Settings</h2>
<style>
    .full-width{width: 100%;}
</style>
<form method="post" action="options.php">
    <?php settings_fields( 'ytf-settings-group' ); ?>
    <?php do_settings_sections( 'ytf-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Google Map API</th>
        <td><input type="text" name="google_map_api" value="<?php echo esc_attr( get_option('google_map_api') ); ?>" class="full-width"/></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Yelp Consumer Key</th>
        <td><input type="text" name="yelp_consumer_key" value="<?php echo esc_attr( get_option('yelp_consumer_key') ); ?>" class="full-width"/></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Yelp Consumer Secret</th>
        <td><input type="text" name="yelp_consumer_secret" value="<?php echo esc_attr( get_option('yelp_consumer_secret') ); ?>" class="full-width"/></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Yelp Token</th>
        <td><input type="text" name="yelp_token" value="<?php echo esc_attr( get_option('yelp_token') ); ?>" class="full-width"/></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Yelp Token Secret</th>
        <td><input type="text" name="yelp_token_secret" value="<?php echo esc_attr( get_option('yelp_token_secret') ); ?>" class="full-width"/></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>