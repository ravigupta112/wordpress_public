<?php

/**
 * Yelp API v2.0 code sample.
 *
 * This program demonstrates the capability of the Yelp API version 2.0
 * by using the Search API to query for businesses by a search term and location,
 * and the Business API to query additional information about the top result
 * from the search query.
 * 
 * Please refer to http://www.yelp.com/developers/documentation for the API documentation.
 * 
 * This program requires a PHP OAuth2 library, which is included in this branch and can be
 * found here:
 *      http://oauth.googlecode.com/svn/code/php/
 * 
 * Sample usage of the program:
 * `php sample.php --term="bars" --location="San Francisco, CA"`
 */
// Enter the path that the oauth library is in relation to the php file
require_once('lib/OAuth.php');

class Yelp_v2 {

// Set your OAuth credentials here  
// These credentials can be obtained from the 'Manage API Access' page in the
// developers documentation (http://www.yelp.com/developers)
    public $consumerKey;
    public $consumerSecret;
    public $token;
    public $tokenSecret;
    public $apiHost = 'api.yelp.com';
    public $defaultTerm = 'dinner';
    public $defaultLocation = 'San Francisco, CA';
    public $searchLimit = 20;
    public $searchPath = '/v2/search/';
    public $businessPath = '/v2/business/';

    public function __construct() {
        $this->consumerKey = get_option('yelp_consumer_key');
        $this->consumerSecret = get_option('yelp_consumer_secret');
        $this->token = get_option('yelp_token');
        $this->tokenSecret = get_option('yelp_token_secret');
    }

    /**
     * Makes a request to the Yelp API and returns the response
     * 
     * @param    $host    The domain host of the API 
     * @param    $path    The path of the APi after the domain
     * @return   The JSON response from the request      
     */
    public function request($host, $path) {
        $unsigned_url = "https://" . $host . $path;

        // Token object built using the OAuth library
        $token = new OAuthToken($this->token, $this->tokenSecret);

        // Consumer object built using the OAuth library
        $consumer = new OAuthConsumer($this->consumerKey, $this->consumerSecret);

        // Yelp uses HMAC SHA1 encoding
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

        $oauthrequest = OAuthRequest::from_consumer_and_token(
                        $consumer, $token, 'GET', $unsigned_url
        );

        // Sign the request
        $oauthrequest->sign_request($signature_method, $consumer, $token);

        // Get the signed URL
        $signed_url = $oauthrequest->to_url();

        // Send Yelp API Call
        try {
            $ch = curl_init($signed_url);
            if (FALSE === $ch)
                throw new Exception('Failed to initialize');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);

            if (FALSE === $data)
                throw new Exception(curl_error($ch), curl_errno($ch));
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 != $http_status)
                throw new Exception($data, $http_status);

            curl_close($ch);
        } catch (Exception $e) {
            trigger_error(sprintf(
                            'Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
        }

        return $data;
    }

    /**
     * Query the Search API by a search term and location 
     * 
     * @param    $term        The search term passed to the API 
     * @param    $location    The search location passed to the API 
     * @return   The JSON response from the request 
     */
    public function search($term, $location, $radious = "", $limit = 0) {
        $url_params = array();

        $url_params['term'] = $term ?: $this->defaultTerm;
        $url_params['limit'] = ($limit > 0) ? $limit : $this->searchLimit;

        if ($radious != "") {
            $url_params['ll'] = $radious;
        } else {
            $url_params['location'] = $location ?: $this->defaultLocation;
        }
        $search_path = $this->searchPath . "?" . http_build_query($url_params);
        return $this->request($this->apiHost, $search_path);
    }

    /**
     * Query the Business API by business_id
     * 
     * @param    $business_id    The ID of the business to query
     * @return   The JSON response from the request 
     */
    public function get_business($business_id) {
        $business_path = $this->businessPath . urlencode($business_id);
        return $this->request($this->apiHost, $business_path);
    }

    /**
     * Queries the API by the input values from the user 
     * 
     * @param    $term        The search term to query
     * @param    $location    The location of the business to query
     */
    public function query_api($term, $location) {
        $response = json_decode($this->search($term, $location));
        print_r($response);
        exit;
        $business_id = $response->businesses[0]->id;

        print sprintf(
                        "%d businesses found, querying business info for the top result \"%s\"\n\n", count($response->businesses), $business_id
        );

        $response = get_business($business_id);

        print sprintf("Result for business \"%s\" found:\n", $business_id);
        print "$response\n";
    }

}
