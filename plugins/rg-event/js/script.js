(function( $ ) {
 
    $( '#rge-event-start-date' ).datepicker({
        dateFormat: 'MM dd, yy',
        onClose: function( selectedDate ){
            $( '#rge-event-end-date' ).datepicker( 'option', 'minDate', selectedDate );
        }
    });
    $( '#rge-event-end-date' ).datepicker({
        dateFormat: 'MM dd, yy',
        onClose: function( selectedDate ){
            $( '#rge-event-start-date' ).datepicker( 'option', 'maxDate', selectedDate );
        }
    });
 
})( jQuery );