<?php
/**
 * Plugin Name: RG Events
 * Plugin URI: https://rgknowledgebase.wordpress.com
 * Description: A plugin to create the events
 * Version: 1.0
 * Author: Ravi Gupta <ravigupta112@hotmail.com>
 * Author URI: https://rgknowledgebase.wordpress.com
 */
define('RG_EVENT_ROOT', plugins_url('', __FILE__));
define('RG_EVENT_IMAGES', RG_EVENT_ROOT . '/img/');
define('RG_EVENT_STYLES', RG_EVENT_ROOT . '/css/');
define('RG_EVENT_SCRIPTS', RG_EVENT_ROOT . '/js/');
require_once( __DIR__.'/widget-upcoming-events.php' );
function rge_custom_post_type()
{
    $labels = array(
        'name' => __('Events', 'rge'),
        'singular_name' => __('Event', 'rge'),
        'add_new_item' => __('Add New Event', 'rge'),
        'all_items' => __('All Events', 'rge'),
        'edit_item' => __('Edit Event', 'rge'),
        'new_item' => __('New Event', 'rge'),
        'view_item' => __('View Event', 'rge'),
        'not_found' => __('No Events Found', 'rge'),
        'not_found_in_trash' => __('No Events Found in Trash', 'rge')
    );

    $supports = array(
        'title',
        'editor',
        'excerpt'
    );

    $args = array(
        'label' => __('Events', 'rge'),
        'labels' => $labels,
        'description' => __('A list of upcoming events', 'rge'),
        'public' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-calendar-alt',
        'has_archive' => true,
        'rewrite' => true,
        'supports' => $supports
    );

    register_post_type('event', $args);
    register_taxonomy("event-type", array("event"), array("hierarchical" => true, "label" => "Event Types", "singular_label" => "Event Type", "rewrite" => true));
}

add_action('init', 'rge_custom_post_type');

function rge_widget_style()
{
    if (is_active_widget('', '', 'rge_upcoming_events', true)) {
        wp_enqueue_style(
                'upcoming-events', STYLES . 'style.css', false, '1.0', 'all'
        );
    }
}

add_action('wp_enqueue_scripts', 'rge_widget_style');

function rge_activation_callback() {
    rge_custom_post_type();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'rge_activation_callback' );

function rge_add_event_info_metabox()
{
    add_meta_box(
            'rge-event-info-metabox', __('Event Info', 'rge'), 'rge_render_event_info_metabox', 'event', 'side', 'core'
    );
}

add_action('add_meta_boxes', 'rge_add_event_info_metabox');

function rge_render_event_info_metabox($post)
{

    // generate a nonce field
    wp_nonce_field(basename(__FILE__), 'rge-event-info-nonce');

    // get previously saved meta values (if any)
    $event_start_date = get_post_meta($post->ID, 'event-start-date', true);
    $event_end_date = get_post_meta($post->ID, 'event-end-date', true);
    $event_venue = get_post_meta($post->ID, 'event-venue', true);

    // if there is previously saved value then retrieve it, else set it to the current time
    $event_start_date = !empty($event_start_date) ? date('F d, Y', $event_start_date) : "";

    //we assume that if the end date is not present, event ends on the same day
    $event_end_date = !empty($event_end_date) ? date('F d, Y', $event_end_date) : "";
    ?>

    <label for="rge-event-start-date"><?php _e('Event Start Date:', 'rge'); ?></label>
    <input class="widefat rge-event-date-input" id="rge-event-start-date" type="text" name="rge-event-start-date" placeholder="Month dd, yyyy" value="<?php echo $event_start_date; ?>" />

    <label for="rge-event-end-date"><?php _e('Event End Date:', 'rge'); ?></label>
    <input class="widefat rge-event-date-input" id="rge-event-end-date" type="text" name="rge-event-end-date" placeholder="Month dd, yyyy" value="<?php echo $event_end_date; ?>" />

    <label for="rge-event-venue"><?php _e('Event Venue:', 'rge'); ?></label>
    <input class="widefat" id="rge-event-venue" type="text" name="rge-event-venue" placeholder="" value="<?php echo $event_venue; ?>" />
    <br >
    <?php
}

function rge_admin_script_style($hook)
{
    global $post_type;

    if (( 'post.php' == $hook || 'post-new.php' == $hook ) && ( 'event' == $post_type )) {
        wp_enqueue_script(
                'upcoming-events', RG_EVENT_SCRIPTS . 'script.js', array('jquery', 'jquery-ui-datepicker'), '1.0', true
        );

        wp_enqueue_style(
                'jquery-ui-calendar', RG_EVENT_STYLES . 'jquery-ui.min.css', false, '1.10.4', 'all'
        );
    }
}

add_action('admin_enqueue_scripts', 'rge_admin_script_style');

function rge_save_event_info($post_id)
{

    // checking if the post being saved is an 'event',
    // if not, then return
    if ('event' != $_POST['post_type']) {
        return;
    }

    // checking for the 'save' status
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = ( isset($_POST['rge-event-info-nonce']) && ( wp_verify_nonce($_POST['rge-event-info-nonce'], basename(__FILE__)) ) ) ? true : false;

    // exit depending on the save status or if the nonce is not valid
    if ($is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }

    // checking for the values and performing necessary actions
    if (isset($_POST['rge-event-start-date'])) {
        update_post_meta($post_id, 'event-start-date', strtotime($_POST['rge-event-start-date']));
    }

    if (isset($_POST['rge-event-end-date'])) {
        update_post_meta($post_id, 'event-end-date', strtotime($_POST['rge-event-end-date']));
    }

    if (isset($_POST['rge-event-venue'])) {
        update_post_meta($post_id, 'event-venue', sanitize_text_field($_POST['rge-event-venue']));
    }
}

add_action('save_post', 'rge_save_event_info');

function rge_custom_columns_head($defaults)
{
    unset($defaults['date']);

    $defaults['event_start_date'] = __('Start Date', 'rge');
    $defaults['event_end_date'] = __('End Date', 'rge');
    $defaults['event_venue'] = __('Venue', 'rge');

    return $defaults;
}

add_filter('manage_edit-event_columns', 'rge_custom_columns_head', 10);

function rge_custom_columns_content($column_name, $post_id)
{

    if ('event_start_date' == $column_name) {
        $start_date = get_post_meta($post_id, 'event-start-date', true);
        echo date('F d, Y', $start_date);
    }

    if ('event_end_date' == $column_name) {
        $end_date = get_post_meta($post_id, 'event-end-date', true);
        echo date('F d, Y', $end_date);
    }

    if ('event_venue' == $column_name) {
        $venue = get_post_meta($post_id, 'event-venue', true);
        echo $venue;
    }
}

add_action('manage_event_posts_custom_column', 'rge_custom_columns_content', 10, 2);
